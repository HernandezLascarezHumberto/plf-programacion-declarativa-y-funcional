# PROGRAMACION LOGIA Y FUNCIONAL.

## PROGRAMACION DECLARATIVA Y FUNCIONAL. 

### PROGRAMACION DECLARATIVA. ORIENTACIONES Y PAUTAS PARA EL ESTUDIO(1998, 1999, 2001).

```plantuml

@startmindmap
*[#08F79C] Programación declarativa
**[#3DF5CB] Paradigma de programación 
***[#6DD7B8] Basado en desarrollo de programas
****[#A4E8D4] Especificado
****[#A4E8D4] Declarativo
***[#6DD7B8] Funciona a nivel de abstraccion muy alto
****[#A4E8D4] Manejo de margen de optimizacion
**[#3DF5CB] Leguajes de programacion declarativa más conocidos
***[#6DD7B8] Lisp
***[#6DD7B8] Haskell
***[#6DD7B8] Prolog 
***[#6DD7B8] Miranda
***[#6DD7B8] SQL (en un sentido amplio)
**[#3DF5CB] Considera los programas como teorias
***[#6DD7B8] Cálculos como deducciones
***[#6DD7B8] Lógica formal
****[#A4E8D4] Lógica de primer orden
****[#A4E8D4] Lógicas clásicas
****[#A4E8D4] Lógica modal y no clásicas
****[#A4E8D4] Lógica algebraica
**[#3DF5CB] Simplifica la escritura de programas paralelos
***[#6DD7B8] Indicar como resolver un problema mediante sentencias
****[#A4E8D4] Trabaja en una forma descriptiva 
**[#3DF5CB] Se divide en dos paradigmas
***[#6DD7B8] Programación Lógica 
****[#A4E8D4] Basado en la logica de primer orden 
****[#A4E8D4] Gira en torno al concepto de predicado
****[#A4E8D4] Caracteristicas del paradigma 
*****[#C5C8C7] Unificación de términos
*****[#C5C8C7] Mecanismos de inferencia automatica
*****[#C5C8C7] Recursión como estructura de control básica
*****[#C5C8C7] Visión lógica de la computación  
****[#A4E8D4] Estudia el uso de la lógica para el planteamiento de problemas 
*****[#C5C8C7] Control sobre las reglas de inferencia  
***[#6DD7B8] Programacion Funcional
****[#A4E8D4] Se basa en el concepto de función 
****[#A4E8D4] Corte matematíco
****[#A4E8D4] Caracteristicas del paradigma
*****[#C5C8C7] Funciones matemáticas puras, sin estado interno ni efectos laterales
*****[#C5C8C7] Valores inmutables
*****[#C5C8C7] Uso profuso de la recursión en la definicion de las funciones
*****[#C5C8C7] Uso de listas como estructuras de datos 
*****[#C5C8C7] Funciones como tipos de datos primitivos 
@endmindmap
```                   
### Lenguaje de programación funcional (2015)
```plantuml
@startmindmap
*[#08F79C] Lenguaje de programacion funcional 
**[#3DF5CB] Paradigma de programación declarativa 
***[#6DD7B8] Basado en el uso de verdaderas funciones matemáticas
****[#A4E8D4] Pueden crearse funciones de orden superior
****[#A4E8D4] Funciones son ciudadanas de primera clase 
*****[#C5C8C7] Expreciones pueden ser asignadas a variables
**[#3DF5CB] Programacion Funcional
***[#6DD7B8] Es un lenguaje expresivo y matemáticamente elegante
***[#6DD7B8] Priorizan el uso de recursividad y aplicacion de funciones 
***[#6DD7B8] Pueden ser vistos elaboraciones del calculo lambda
**[#3DF5CB] Lenguajes funcionales 
***[#6DD7B8] Han sido utilizados en aplicaciones
****[#A4E8D4] Comerciales
****[#A4E8D4] Industrias
***[#6DD7B8] Tipos de lenguajes
****[#A4E8D4] Haskell 
****[#A4E8D4] Scheme 
****[#A4E8D4] Lisp
****[#A4E8D4] Scala 
****[#A4E8D4] Miranda 
****[#A4E8D4] R
**[#3DF5CB] Caracteristicas 
***[#6DD7B8] Se considera una funcion matemática  
***[#6DD7B8] Describe una relacion entre una entrada y una salida
***[#6DD7B8] El concepto de estado o variable se elimina completamente 
***[#6DD7B8] Permite crear programas con el enfoque de una función de la función
***[#6DD7B8] Función mediante los parámetros formales
***[#6DD7B8] Ausenci de efectos colaterales 
***[#6DD7B8] El valor de una expresión solo depende de los valores de sus subexpresiones
***[#6DD7B8] Las variables de una zona de memoria de la funci
***[#6DD7B8] Lugar donde se almacena un valor
**[#3DF5CB] Consecuencia de ausencia de variables y asignación
***[#6DD7B8] Transparencia referencial
****[#A4E8D4] Cualquier función no depende del orden de evaluación de sus parámetros
****[#A4E8D4] El valor de cualquier función depende solamente de los valores de sus parámetros
****[#A4E8D4] Facilita el empleo de la programación funcional 
***[#6DD7B8] No existe estado
****[#A4E8D4] No existe concepto de localizacion
*****[#C5C8C7] Memoria
*****[#C5C8C7] Variable
***[#6DD7B8] El ambiente de ejecucion
****[#A4E8D4] Asocia valores con nombres
*****[#C5C8C7] Con localizaciones de Memoria
****[#A4E8D4] Semántica de almacenamiento o semántica de apuntadores
***[#6DD7B8] Manipulación funciones sin restricciones arbitrarias
****[#A4E8D4] Las funciones deben ser vistas como valores en si
****[#A4E8D4] Pueden ser ejecutadas por otras funciones 
****[#A4E8D4] Pueden ser tambien parametros de otras funciones 
@endmindmap
```